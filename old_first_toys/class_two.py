class client:
    def __init__(self, first_deposit, client_number):
        self.saldo = first_deposit
        self.number = client_number

    def do_deposit(self, amount):
        #equivalent to self.saldo = self.saldo + amount
        self.saldo += amount

    def do_withraw(self, amount):
        if amount > self.saldo:
            print("No puedes retirar ", amount, "pesos")

        else:
            #equivalent to self.saldo = self.saldo - amount
            self.saldo -= amount

class employee(client):
    def __init__(self, first_deposit, client_number, employee_number):
        self.saldo = first_deposit
        self.number = client_number
        self.employee_n = employee_number

    def get_e_number(self):
        return self.employee_n

if __name__ == '__main__':
    tuti = client(50, 5)
    puro = client(500, 2)
    print('tuti tiene ', tuti.saldo, ' pesos "chilenos" ')
    print("puro tiene ", puro.saldo, " pesos 'mexicanos' ")

    puro.do_withraw(2000)
    tuti.do_deposit(30)
    print('tuti tiene ', tuti.saldo, ' pesos "chilenos" ')
    print("puro tiene ", puro.saldo, " pesos 'mexicanos' ")


    pedro = employee(600, 3, 1)
    print("pedro tiene ", pedro.saldo, " pesos 'mexicanos' ")
    print("pedro es el empleado numero", pedro.get_e_number() )


