a = 3

if a < 3: # condicion
    print("a es menor que 3") # ejecucion 1

elif a > 3:
    print("a es mayor que 3") # ejecucion 2

else:
    print("supongamos que es igual a 3") # ejecucion 3

print("esta linea corre, sin importar las condiciones ")
