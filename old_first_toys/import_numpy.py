from numpy import sin
if __name__ == "__main__":
    sn_14p = sin(3.14159 / 4.0)
    print("sin(pi * (1/4)) =", sn_14p)
