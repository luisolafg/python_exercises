class a:
    '''
        clase ejemplo
    '''
    #x = 8 # this will be overwritten

    def __init__(self, x_in):
        self._z = x_in
        print("beginning")

    def __call__(self):
        print("each time call")
        if self._z > 5:
            return self._z

        print("NOT greater than 5")

    def imprime(self):
        print("z =", self._z)


if __name__ == "__main__":
    b = a(4)
    #print(b._z) # bad practice
    b.imprime()
    z_out = b()
    print("z_out =", z_out)
    #b()
    #b()

    #print(dir(b))

