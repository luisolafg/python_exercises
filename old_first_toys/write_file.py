if __name__ == "__main__":
    source_file = ["a = 5",
                   "b = 6",
                   "esto es una cadena de caracteres"]

    print(source_file)

    f = open("ejemplo.txt", "w")
    for line_str in source_file:
        f.write(line_str + "\n")

    f.close()
