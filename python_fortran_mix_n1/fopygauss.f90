program fopygauss
    real:: z(0:100,0:100)
    open(unit=1, status='old',file = 'datos_in.dat', action='read')
    open(unit=2, status='replace', file='Gauss.dat', action='write')
    read (1,*) x0, y0, zmax, width
    close(1)
    write(*,'(A,5F6.2)') " Data inside Fortran = ", x0, y0, zmax, width
    write (*,*) 'Modelling the Gaussian bell with Fortran'
    width2 = width**2
    do i = 0, 100
        x = real(i)
        do j = 0, 100
            y = real(j)
            dist2 = (x - x0)**2 + (y - y0)**2
            z(i, j) = zmax * exp(-2.7725887*(dist2/width2))
        end do
    end do
    write(*,*) "Fortran writing file 'Gauss.dat' "
    do i = 0, 100
        write (2,'(101F6.2)') (z(i,j), j = 0, 100)
    end do
    close(2)
end program fopygauss
